#ifndef JOBDEV_H
#define JOBDEV_H

#include <QMainWindow>

namespace Ui {
class Jobdev;
}

class Jobdev : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit Jobdev(QWidget *parent = 0);
    ~Jobdev();
    
private slots:

private:
    Ui::Jobdev *ui;
};

#endif // JOBDEV_H
