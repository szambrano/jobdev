#ifndef VENTANA_H
#define VENTANA_H

class ventana
{
private:
    double porcentaje_ancho;
    double porcentaje_largo;
    int ancho_monitor;
    int largo_monitor;
    int ancho_ventana;
    int largo_ventana;
    int posicion_x;
    int posicion_y;
public:
    ventana ();
    int get_ancho_ventana();
    int get_largo_ventana();
    int get_posicion_x();
    int get_posicion_y();
    void informacion_ventana();
    void calculos(int ancho_monitor, int largo_monitor, double porcentaje_ancho, double porcentaje_largo, int x, int y);

};



#endif // VENTANA_H
