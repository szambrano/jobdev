#include <QApplication>
#include <QDesktopWidget>
#include "QTextCodec"
#include "jobdev.h"
#include <iostream>
#include <ventana.h>
#include <fstream>

using namespace std;
int main(int argc, char *argv[])
{
    //Codificacion UTF8
    QTextCodec *codificacion_utf8 = QTextCodec::codecForName("UTF-8");
    QTextCodec::setCodecForTr(codificacion_utf8);
    QTextCodec::setCodecForCStrings(codificacion_utf8);
    QTextCodec::setCodecForLocale(codificacion_utf8);

    QApplication a(argc, argv);
    //Se crea el objeto ventana y se calcula el tamaño de la ventana y la posicion
    QDesktopWidget *monitor = QApplication::desktop();
    ventana principal;
    principal.calculos(monitor->width(), monitor->height(),0.70,0.40,2,2);
    principal.informacion_ventana();

    //Creamos el archivo de configuracion.
    ofstream configuraciones("config/configuraciones.txt");

    //Escribimos las lineas de configuracion de la ventana
    configuraciones << "Ancho de Ventana: " << "|" << principal.get_ancho_ventana() << endl;
    configuraciones << "Largo de Ventana: " << "|" << principal.get_largo_ventana() << endl;
    configuraciones.close();

    //GUI
    Jobdev w;
    w.setGeometry(principal.get_posicion_x(), principal.get_posicion_y() ,principal.get_ancho_ventana(),principal.get_largo_ventana());
    w.show();

    return a.exec();
}
