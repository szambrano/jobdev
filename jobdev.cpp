#include "jobdev.h"
#include "ui_jobdev.h"
#include "ventana.h"
#include <iostream>
#include <fstream>

using namespace std;
Jobdev::Jobdev(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Jobdev)
{
    ui->setupUi(this);
    char cadena[300];
    QString dato_configuracion_ventana;
    QStringList configuracion_ventana[2];
    ifstream configuraciones("config/configuraciones.txt");
    int i;
    for (i=0; i<2; i++)
     {
         configuraciones.getline(cadena, 300);
         dato_configuracion_ventana = cadena;
         // En la lista de arreglos guardamos el ancho y la ventana que sacamos del .txt
         configuracion_ventana[i] = dato_configuracion_ventana.split("|");
         //cout << configuracion_ventana[i][2].toStdString() << endl;
     }
    //Ahora vamos a calcular el tamaño de la tabla
    ventana tabla_joddev;
    tabla_joddev.calculos(configuracion_ventana[0][1].toInt(),configuracion_ventana[1][1].toInt(),0.02,0.20,2,3);
    tabla_joddev.informacion_ventana();
    ui->tabla_principal->setGeometry(tabla_joddev.get_posicion_x(),tabla_joddev.get_posicion_y(),tabla_joddev.get_ancho_ventana(),tabla_joddev.get_largo_ventana());

    //Ahora vamos a calcular el tamaño del LineEdit
    ventana line_edit;
    line_edit.calculos(configuracion_ventana[0][1].toInt(),configuracion_ventana[1][1].toInt(),0,0.94,150,150);
    ui->line_edit->setGeometry(line_edit.get_posicion_x(),line_edit.get_posicion_y(),line_edit.get_ancho_ventana(),line_edit.get_largo_ventana());

}

Jobdev::~Jobdev()
{
    delete ui;
}


