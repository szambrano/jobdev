#-------------------------------------------------
#
# Project created by QtCreator 2014-05-16T18:51:59
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Jobdev
TEMPLATE = app


SOURCES += main.cpp\
        jobdev.cpp \
    ventana.cpp

HEADERS  += jobdev.h \
    ventana.h

FORMS    += jobdev.ui
