#include <QApplication>
#include <iostream>
#include <ventana.h>

using namespace std;

ventana::ventana()
{

}

void ventana::calculos(int ancho_monitor, int largo_monitor, double porcentaje_ancho, double porcentaje_largo, int x, int y)
{
    //Inicializamos las variables
    this->porcentaje_ancho = porcentaje_ancho;
    this->porcentaje_largo = porcentaje_largo;
    this->ancho_monitor = ancho_monitor;
    this->largo_monitor = largo_monitor;
    //Se calcula el Ancho de la Ventana
    this->ancho_ventana = this->ancho_monitor * this->porcentaje_ancho;
    this->ancho_ventana = this->ancho_monitor - this->ancho_ventana;
    //Se calcula el Largo de la Ventana
    this->largo_ventana = this->largo_monitor * this->porcentaje_largo;
    this->largo_ventana = this->largo_monitor - this->largo_ventana;
    //Se calcula la posicion X & Y en el plano.
    this->posicion_x = (this->ancho_monitor - this->ancho_ventana) / x;
    this->posicion_y = (this->largo_monitor - this->largo_ventana) / y;
}


int ventana::get_ancho_ventana()
{ return this->ancho_ventana; }


int ventana::get_largo_ventana()
{ return this->largo_ventana;}

int ventana::get_posicion_x()
{ return this->posicion_x;}

int ventana::get_posicion_y()
{ return this->posicion_y;}



void ventana::informacion_ventana()
{
    cout << "================= INFORMACION ==============" << endl;
    cout << "Ancho: " << this->ancho_monitor << " X " << "Largo " << this->largo_monitor << endl;
    cout << "Ancho Ventana " << this->ancho_ventana << endl;
    cout << "Largo Ventana " << this->largo_ventana << endl;
    cout << "Posicion en Coordenadas X: " << this->posicion_x << endl;
    cout << "Posicion en Coordenadas Y: " << this->posicion_y << endl;
}

